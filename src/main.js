import '@/assets/scss/app.scss';
import Vue from 'vue';
import Router from 'vue-router';
import axios from 'axios';

import BootstrapVue from 'bootstrap-vue';
import ElementUI from 'element-ui';

import App from './App.vue';
import router from './router';

Vue.use(BootstrapVue);
Vue.use(ElementUI, { size: 'small', zIndex: 3000 });
Vue.use(Router);

let loading;

axios.interceptors.request.use((config) => {
  loading = ElementUI.Loading.service();
  return config;
}, (errorRequest) => {
  loading.close();
  return Promise.reject(errorRequest);
});

axios.interceptors.response.use((response) => {
  loading.close();
  return response;
}, (errorResponse) => {
  loading.close();
  return Promise.reject(errorResponse);
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
