import Router from 'vue-router';
import Base from './Base.vue';
import Home from './views/Home.vue';
import Atividade1 from './views/Atividade1.vue';
import Atividade2 from './views/Atividade2.vue';
import Atividade3 from './views/Atividade3.vue';

const routes = [
  {
    path: '/',
    redirect: '/home',
    component: Base,
    children: [
      {
        path: '/home',
        name: 'home',
        component: Home
      },
      {
        path: '/Atividade1',
        name: 'Atividade1',
        component: Atividade1
      },
      {
        path: '/Atividade2',
        name: 'Atividade2',
        component: Atividade2
      },
      {
        path: '/Atividade3',
        name: 'Atividade3',
        component: Atividade3
      }
    ]
  }
];

const router = new Router({ routes });

export default router;
